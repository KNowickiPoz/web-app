package webapp.todolist;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import webapp.todolist.TodoListService;


@WebServlet(urlPatterns = "/add-todolist.do")
public class AddServlet extends HttpServlet {
	
	private TodoListService todoService = new TodoListService();
	  
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String newTask = request.getParameter("task");
    	String date = request.getParameter("date");
    	todoService.addTask(new TodoList(newTask, date));
    	
    	response.sendRedirect("/todolist.do");
    }
}