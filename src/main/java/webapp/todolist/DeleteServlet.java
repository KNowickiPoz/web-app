package webapp.todolist;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import webapp.todolist.TodoListService;


@WebServlet(urlPatterns = "/delete-task.do")
public class DeleteServlet extends HttpServlet {
	
	private TodoListService todoService = new TodoListService();
	
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	todoService.deleteTask(new TodoList(request.getParameter("task"), request.getParameter("date")));
    	response.sendRedirect("/todolist.do");
    }
}