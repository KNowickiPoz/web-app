package webapp.todolist;

import java.util.ArrayList;
import java.util.List;

public class TodoListService {
	private static List<TodoList> todos = new ArrayList<TodoList>();
	static {
		todos.add(new TodoList("Make a calendar","21.07.1958"));
		todos.add(new TodoList("Find a new job","11.01.2016"));
		todos.add(new TodoList("Perform a triumph dance","15.04.2016"));
	}
	
	public List<TodoList> retrieveTodos() {
		return todos;
	}
	
	public void addTask(TodoList task) {
		todos.add(task);
	}
	
	public void deleteTask(TodoList task) {
		todos.remove(task);
	}
	
}
