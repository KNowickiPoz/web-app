package webapp.todolist;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import webapp.todolist.TodoListService;


@WebServlet(urlPatterns = "/todolist.do")
public class TodoListServlet extends HttpServlet {
	
	private TodoListService todoService = new TodoListService();
	
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	request.getSession().setAttribute("todos", todoService.retrieveTodos());
        request.getRequestDispatcher("WEB-INF/views/todolist.jsp").forward(request, response);
    }
    
}