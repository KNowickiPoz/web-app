<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

    
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link href="webjars/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
		
		<title>Calendar - tasks</title>
	
		<style>
		    .footer {
		    	position: absolute;
		    	text-align: center;
		        bottom: 0;
		        width: 100%;
		        height: 60px;
		        background-color: #b4b4b4;
		    }
		</style>
	
	</head>

	<body>
		<div class="container">
			<form action = "/login.do" method = "post">
				<p><font color = "red">${error}</font></p>
				Your name here: <input type = "text" name = "name"/>
				Password: <input type = "password" name = "password"/> <input type = "submit" value = "Send"/>
			</form>
		</div>
		
		<footer class="footer">
        	<p>Disclaimer: when you're reading this code, somewhere in the world EVIII is being shot</p>
    	</footer>
	
	<script src="webjars/jquery/1.9.1/jquery.min.js"></script>
	<script src="webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	
	</body>
</html>