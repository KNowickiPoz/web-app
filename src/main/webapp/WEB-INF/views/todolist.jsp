<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

    
<!DOCTYPE html>
<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link href="webjars/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	
	<title>Calendar - tasks</title>
	
		<style>
		    .footer {
		    	position: absolute;
		    	text-align: center;
		        bottom: 0;
		        width: 100%;
		        height: 60px;
		        background-color: #b4b4b4;
		    }
		</style>
	
	</head>

	<body>
	
		<nav class="navbar navbar-default">
	
	        <a href="#" class="navbar-brand">Calendar</a>
	
	        <ul class="nav navbar-nav">
	            <li class="active"><a href="#">Home</a></li>
	            <li><a href="/todolist.do">My Tasks</a></li>
	            <li><a href="http://www.slodkabulka.pl">Here's little something about me...</a></li>
	        </ul>
	
	        <ul class="nav navbar-nav navbar-right">
	            <li><a href="/logout.do">Logout</a></li>
	        </ul>
	
	    </nav>
	    
	    <div class="container">
			<p><h1>Hello ${name}</h1></p>
			<div class="panel panel-default panel-primary">
				<div class="panel-body">
					<h3>Your tasks are:</h3>
				</div>
			</div>
			<ul class="list-group">
				<c:forEach items="${todos}" var="task">
					<li class="list-group-item">${task.name} -&nbsp;${task.date} <a class="btn btn-danger" href="/delete-task.do?task=${task.name}&date=${task.date}">Remove</a></li>
				</c:forEach>
			</ul>
			<form action="/add-todolist.do" method="post">
			<fieldset class="form-group">
				<label>What to do:</label>
				<input type="text" name="task"/><br/>
			</fieldset>
			<fieldset class="form-group">
				<label>Date (dd.mm.yyyy):</label> 
				<input type="text" name="date"/><br/>
			</fieldset>
				<input class="btn btn-success" type="submit" value="Add task"/>
			</form>
		</div>
		
		<footer class="footer">
	        <p>Disclaimer: when you're reading this code, somewhere in the world Ep. VIII is being shot</p>
	    </footer>
		
		<script src="webjars/jquery/1.9.1/jquery.min.js"></script>
		<script src="webjars/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	</body>
</html>